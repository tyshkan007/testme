package ry.teccer.testme;

import android.os.Parcelable;

/**
 * Created by Anton Alexandrov on 28.07.15.
 */
public interface IServer extends Parcelable {

    String getHost();

    int getPort();

    String getUrlAndPort();

    String getLogin();

    String getPassword();


}
