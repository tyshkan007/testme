package ry.teccer.testme;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Anton Alexandrov on 28.07.15.
 */
public class ModelCameras implements Parcelable {

    private IServer server;
    public String name;
    public String uri;
    public int height;
    public int width;

    public ModelCameras() {
        this.server = Main.server;
    }

    public ModelCameras(String name, String uri, int height, int width) {
        this.server = Main.server;
        this.name = name;
        this.uri = uri;
        this.height = height;
        this.width = width;
    }

    protected ModelCameras(Parcel in) {
        server = (IServer) in.readValue(Server.class.getClassLoader());
        name = in.readString();
        uri = in.readString();
        height = in.readInt();
        width = in.readInt();
    }

    public IServer getServer() {
        return server;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(server);
        dest.writeString(name);
        dest.writeString(uri);
        dest.writeInt(height);
        dest.writeInt(width);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ModelCameras> CREATOR = new Parcelable.Creator<ModelCameras>() {
        @Override
        public ModelCameras createFromParcel(Parcel in) {
            return new ModelCameras(in);
        }

        @Override
        public ModelCameras[] newArray(int size) {
            return new ModelCameras[size];
        }
    };


    @Override
    public String toString(){
        return server.getUrlAndPort() + uri;
    }


}
