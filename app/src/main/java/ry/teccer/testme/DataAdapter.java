package ry.teccer.testme;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ry.teccer.testme.dbPack.ConstDB;
import ry.teccer.testme.dump.Var;

/**
 * Created by Anton Alexandrov on 05.08.15.
 */
public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewItem> {

    private List<ModelCameras> data;
    private Context context;
    private Cursor cursor;

    public DataAdapter(Context context) {
        this.context = context;
        cursor = context.getContentResolver().query(ConstDB.CAMERA_CONTENT_URI, null, null, null, null);
        context.getContentResolver().registerContentObserver(ConstDB.CAMERA_CONTENT_URI, true, contentObserver);
        setData();
    }

   ContentObserver contentObserver = new ContentObserver(new Handler()) {
       @Override
       public void onChange(boolean selfChange) {
           super.onChange(selfChange);
           cursor = context.getContentResolver().query(ConstDB.CAMERA_CONTENT_URI, null, null, null, null);
           setData();
       }
   };

    private void setData(){
        Var.dump("setData called");
        data = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {

                ModelCameras modelCameras = new ModelCameras();
                // cursor.getInt(cursor.getColumnIndex(ConstDB.ID));
                modelCameras.uri = cursor.getString(cursor.getColumnIndex(ConstDB.URI));
                modelCameras.name = cursor.getString(cursor.getColumnIndex(ConstDB.NAME));
                modelCameras.height = cursor.getInt(cursor.getColumnIndex(ConstDB.HEIGHT));
                modelCameras.width = cursor.getInt(cursor.getColumnIndex(ConstDB.WIDTH));
                data.add(modelCameras);
            } while (cursor.moveToNext());
        }
        notifyDataSetChanged();
    }


    @Override
    public ViewItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        return new ViewItem(v);
    }

    @Override
    public void onBindViewHolder(ViewItem holder, int position) {
        holder.textView.setText(data.get(position).uri);
        holder.textView2.setText(data.get(position).name);
    }

    @Override
    public int getItemCount() {
        if (data == null){
            return 0;
        }
        return data.size();
    }

    public class ViewItem extends RecyclerView.ViewHolder {

        public TextView textView;
        public TextView textView2;

        public ViewItem(final View itemView) {
            super(itemView);

            textView = (TextView) itemView.findViewById(R.id.text);
            textView2 = (TextView) itemView.findViewById(R.id.text1);

        }
    }

}
