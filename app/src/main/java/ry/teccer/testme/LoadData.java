package ry.teccer.testme;

import android.content.ContentValues;
import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import ry.teccer.testme.dbPack.ConstDB;


public class LoadData extends AsyncTaskLoader<Void> {

    final private static String AUT = "Authorization";

    private Server server;
    private boolean load = false;

    public LoadData(Context context) {
        super(context);
    }

    @Override
    public void forceLoad() {
        load = true;
        super.forceLoad();
    }

    @Override
    public Void loadInBackground() {
        StringBuilder buffer = new StringBuilder("");
        try{
            URL url = new URL("http://" + server.getUrlAndPort()  + "/cameras");
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestProperty("User-Agent", "line for android");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty(AUT, "Basic " + Base64.encodeBytes((server.getLogin() + ":" + server.getPassword()).getBytes()));
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = rd.readLine()) != null) {
                buffer.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        parse(buffer.toString());
        load = false;
        return  null;
    }

    private void parse(String s) {
        try {
          //  JSONObject dataJsonObj = new JSONObject(s);
            JSONArray cameras =  new JSONArray(s);
            for (int i = 0; i < cameras.length(); i++) {
                JSONObject camera = cameras.getJSONObject(i);
                ContentValues cv = new ContentValues();
                cv.put("uri", camera.getString("uri"));
                cv.put("name", camera.getString("name"));
                cv.put("height", camera.getString("height"));
                cv.put("width", camera.getString("width"));
                getContext().getContentResolver().insert(ConstDB.CAMERA_CONTENT_URI, cv);
                synchronized(this) {
                    try {
                        wait(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void setServer(Server server) {
        this.server = server;
    }

    public boolean isLoad() {
        return load;
    }


}
