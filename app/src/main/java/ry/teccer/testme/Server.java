package ry.teccer.testme;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Anton Alexandrov on 28.07.15.
 */
public class Server implements IServer {

    private String host;
    private int port;
    private String login;
    private String password;


    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getUrlAndPort(){
        return host + ":" + String.valueOf(port);
    }

    public Server(String host, int port, String login, String password){
        this.host = host;
        this.port = port;
        this.login = login;
        this.password = password;
    }



    protected Server(Parcel in) {
        host = in.readString();
        port = in.readInt();
        login = in.readString();
        password = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(host);
        dest.writeInt(port);
        dest.writeString(login);
        dest.writeString(password);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Server> CREATOR = new Parcelable.Creator<Server>() {
        @Override
        public Server createFromParcel(Parcel in) {
            return new Server(in);
        }

        @Override
        public Server[] newArray(int size) {
            return new Server[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Server server = (Server) o;

        if (port != server.port) return false;
        if (host != null ? !host.equals(server.host) : server.host != null) return false;
        if (login != null ? !login.equals(server.login) : server.login != null) return false;
        return !(password != null ? !password.equals(server.password) : server.password != null);

    }

    @Override
    public int hashCode() {
        int result = host != null ? host.hashCode() : 0;
        result = 31 * result + port;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}