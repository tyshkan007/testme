package ry.teccer.testme;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ProgressBar;
import ry.teccer.testme.dump.Var;

public class Main extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Void>,
        OnClickListener {

    public static Server server;
    private ProgressBar progressBar;
    private Main self;
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private DataAdapter dataAdapter;
    private static final int LOADER_TIME_ID = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        dataAdapter = new DataAdapter(getBaseContext());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        setSupportActionBar(toolbar);
        toolbar.setLogo(R.mipmap.ic_launcher);

        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));//
        recyclerView.setAdapter(dataAdapter);

        if (server == null) {
            server = new Server("demo.devline.ru", 80, "admin", "");
        }
        self = this;
        showHideProgressBar();

    }


    @Override
    public Loader<Void> onCreateLoader(int id, Bundle args) {
        if(id == LOADER_TIME_ID) {
            LoadData loadData = new LoadData(this);
            loadData.setServer(server);
            return loadData;
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Void> loader, Void data) {
        showHideProgressBar();
        //dataAdapter.notifyDataSetChanged();
        Var.dump("count = ", dataAdapter.getItemCount());
    }


    @Override
    public void onLoaderReset(Loader<Void> loader) {
        Var.dump("reset");
        //dataAdapter.setData(null);
    }


    @Override
    public void onClick(View v) {
        Snackbar
                .make(findViewById(android.R.id.content), R.string.loadQ,
                        Snackbar.LENGTH_LONG)
                .setAction(R.string.yes, mOnClickListener)
                .show();
    }

    private void showHideProgressBar(){
        LoaderManager loaderManager = getSupportLoaderManager();
        Loader<Void> loader = loaderManager.getLoader(LOADER_TIME_ID);
        LoadData loadData = (LoadData) loader;
        progressBar.setVisibility(loadData != null && loadData.isLoad() ? View.VISIBLE : View.GONE);
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Loader loader = getSupportLoaderManager().getLoader(LOADER_TIME_ID);
            if(loader == null){
                getSupportLoaderManager().initLoader(LOADER_TIME_ID, null, self).forceLoad();

            } else {
                getSupportLoaderManager().restartLoader(LOADER_TIME_ID, null, self).forceLoad();
            }
            showHideProgressBar();
        }
    };

}
