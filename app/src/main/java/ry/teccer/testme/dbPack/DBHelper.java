package ry.teccer.testme.dbPack;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Anton Alexandrov on 31.07.15.
 */
public class DBHelper extends SQLiteOpenHelper {




    static final String DB_CREATE = "create table " + ConstDB.TABLE_NAME + "("
            + ConstDB.ID + " integer primary key autoincrement, "
            + ConstDB.URI + " text, "
            + ConstDB.NAME + " text, "
            + ConstDB.HEIGHT + " integer, "
            + ConstDB.WIDTH + " integer"
            +");";

    static final String AUTHORITY = "ry.teccer.testme";
    static final String CAMERA_PATH = "camera";

    public DBHelper(Context context) {
        // конструктор суперкласса
        super(context, "testDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DB_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


}
