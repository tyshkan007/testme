package ry.teccer.testme.dbPack;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

/**
 * Created by Anton Alexandrov on 05.08.15.
 */
public class MyDataProvider extends ContentProvider {


    private static final int CAMERA_CODE = 1;
    private static final int CAMERA_ID_CODE = 2;

    private final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private DBHelper dbHelper;

    @Override
    public boolean onCreate() {
        dbHelper = new DBHelper(getContext());
        uriMatcher.addURI(ConstDB.AUTHORITY, ConstDB.CAMERA_PATH, CAMERA_CODE);
        uriMatcher.addURI(ConstDB.AUTHORITY, ConstDB.CAMERA_PATH + "/#", CAMERA_ID_CODE);
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor = null;
        switch (uriMatcher.match(uri)) {
            case CAMERA_CODE:
                cursor = dbHelper.getReadableDatabase().query(
                        ConstDB.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                cursor.setNotificationUri(getContext().getContentResolver(), ConstDB.CAMERA_CONTENT_URI);
                break;
            case  CAMERA_ID_CODE:
                cursor = dbHelper.getReadableDatabase().query(
                        ConstDB.TABLE_NAME,
                        projection,
                        ConstDB.ID + " = ?",
                        new String[]{uri.getLastPathSegment()},
                        null,
                        null,
                        null
                );
                cursor.setNotificationUri(getContext().getContentResolver(), Uri.withAppendedPath(ConstDB.CAMERA_CONTENT_URI, uri.getLastPathSegment()));
                cursor.moveToFirst();
                break;
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if (uriMatcher.match(uri) != CAMERA_CODE)
            return null;
        long rowID = dbHelper.getWritableDatabase().insert(ConstDB.TABLE_NAME, null, values);
        if(rowID == -1){
            return null;
        }
        Uri resultUri = ContentUris.withAppendedId(ConstDB.CAMERA_CONTENT_URI, rowID);
        getContext().getContentResolver().notifyChange(resultUri, null);
        return resultUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

}