package ry.teccer.testme.dbPack;

import android.net.Uri;

/**
 * Created by Anton Alexandrov on 06.08.15.
 */
public class ConstDB {

    public static final String TABLE_NAME = "cameras";
    public static final String ID = "id";
    public static final String URI = "uri";
    public static final String NAME = "name";
    public static final String HEIGHT = "height";
    public static final String WIDTH = "width";

    public static final String AUTHORITY = "ry.teccer.testme";
    public static final String CAMERA_PATH = "camera";
    public static final Uri CAMERA_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + CAMERA_PATH);


}
